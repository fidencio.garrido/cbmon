import pandas as pd
import matplotlib.pyplot as plt
import sys
from pandas.plotting import scatter_matrix

SESSION=sys.argv[1]
BASE_PATH=sys.argv[2]

def mergeFiles():
    CPU_FILE = BASE_PATH + SESSION + '_cpuavg.csv'
    QUERY_FILE = BASE_PATH + SESSION + '_queryCount.csv'
    RT_FILE = BASE_PATH + SESSION + '_responseTime.csv'
    cpu_data = pd.read_csv(CPU_FILE)
    query_data = pd.read_csv(QUERY_FILE)
    response_data = pd.read_csv(RT_FILE)
    tmp = pd.merge(response_data, query_data, on='Time')
    return pd.merge(cpu_data, tmp, on='Time')

QUERY_DATA = mergeFiles()
attributes = ['Query CPU', 'Index CPU', 'Data CPU', 'Average query execution time', 'Queries per second', 'Queries per second per node']
scatter_matrix(QUERY_DATA[attributes], figsize=(12, 7))
plt.savefig('./out/query_scatter.png')

print QUERY_DATA.head()