from matplotlib import pyplot as plt
import csv
import sys
import numpy as np

SESSION=sys.argv[1]
BASE_PATH=sys.argv[2]

QUERY_CPU = []
QUERY_COUNT = []
RESPONSE_TIME_AVG = []
PERCENTAGE_GAP = 5
plt.figure(figsize=(16,9), dpi=150)

CPU_FILE = BASE_PATH + SESSION + '_cpuavg.csv'
QUERY_FILE = BASE_PATH + SESSION + '_queryCount.csv'
RT_FILE = BASE_PATH + SESSION + '_responseTime.csv'

with open(CPU_FILE, 'rb') as f:
    READER = csv.reader(f, delimiter=',')
    QUERY_CPU = [float(x[1]) for i, x in enumerate(READER) if i >0]

with open(QUERY_FILE, 'rb') as f:
    READER = csv.reader(f, delimiter=',')
    QUERY_COUNT = [x[2] for i, x in enumerate(READER) if i >0]

with open(RT_FILE, 'rb') as f:
    READER = csv.reader(f, delimiter=',')
    RESPONSE_TIME_AVG = [x[1] for i, x in enumerate(READER) if i >0]

def countmeIn(n, queryCollection, cpuCollection):
    items = [i for i, val in enumerate(cpuCollection) if val < n and val >= (n-PERCENTAGE_GAP)]
    return len(items)

fig, ax = plt.subplots()
ax.grid(True, zorder=5)

# for percentage, average of every queryCount in that percentage
PERCENTAGE = [x for x in range(101) if x % PERCENTAGE_GAP == 0]
c = [ countmeIn(x, QUERY_COUNT, QUERY_CPU) for x in PERCENTAGE  ]

plt.plot(PERCENTAGE,c, color='#6bc6a0', marker='.', linestyle='solid', label="Count")

# plt.legend(loc='best')

# _, ax = plt.subplots()

# ax.fill(PERCENTAGE, c, zorder=10)
# for i, v in enumerate(c):
#     ax.text(i * 5, v + 1, str(v), color='#6bc6a0', fontweight='bold')

plt.title("Query count vs CPU usage")

plt.ylabel("Active Query Count")
plt.xlabel("CPU %")
	
plt.savefig("./out/queryvscpu.png")
plt.close()

# plot response time vs cpu
plt.figure(figsize=(16,9), dpi=150)
fig, ax = plt.subplots()
ax.grid(True, zorder=5)
RESPONSE_TIME_AVG.append(0)
plt.scatter(QUERY_CPU, RESPONSE_TIME_AVG)
plt.title("Response time vs CPU")
plt.ylabel("Response Time (ms)")
plt.xlabel("CPU %")
plt.savefig("./out/rtvscpu.png")
plt.close()


# plot response time vs cpu
plt.figure(figsize=(16,9), dpi=150)
fig, ax = plt.subplots()
ax.grid(True, zorder=5)
# Categorize response time in this buckets
BUCKETS = [10, 50, 100, 200, 500, 1000, 1500, 2000, 3000, 5000]
QUERY_COUNT.append(0)
QUERY_COUNT.append(0)
plt.scatter(QUERY_COUNT, RESPONSE_TIME_AVG, marker='.')
plt.title("Response time vs Queries per second")
plt.ylabel("Response Time (ms)")
plt.xlabel("Completed queries per node")
plt.savefig("./out/rtvscount.png")
plt.close()


# plot completed Queries per node vs cpu
plt.figure(figsize=(16,9), dpi=150)
fig, ax = plt.subplots()
ax.grid(True, zorder=5)
# Categorize response time in this buckets
plt.scatter(QUERY_COUNT, QUERY_CPU, marker='.')

plt.title("Queries per second vs CPU")
plt.ylabel("CPU %")
plt.xlabel("Completed queries per node")
plt.savefig("./out/cpuvscount.png")
plt.close()