from matplotlib import pyplot as plt
import csv
import sys
import numpy as np

SESSION=sys.argv[1]
BASE_PATH=sys.argv[2]

BUCKETS = []
STATS = []
BUCKETS_STATS = dict()
plt.figure(figsize=(16,9), dpi=150, frameon=True)

BUCKETS_FILE = BASE_PATH + SESSION + '_buckets.txt'
STATS_FILE = BASE_PATH + SESSION + '_bucketstats.csv'

with open(BUCKETS_FILE, 'rb') as f:
    READER = csv.reader(f, delimiter=',')
    _BUCKETS = [x for x in READER]
    BUCKETS = [x for x in _BUCKETS[0]]

with open(STATS_FILE, 'rb') as f:
    READER = csv.reader(f, delimiter=',')
    STATS = [x for x in READER]

def splitBucketStats(bucket):
    BUCKET_STATS = [x for x in STATS if x[1] == bucket]
    BUCKETS_STATS[bucket] = BUCKET_STATS

def defaultTx(item):
    return item

def memoryTx(mem):
    return float(mem) / 1024 / 1024
    
def createChart(title, ylabel, columnIndex, filename, transformFunction):
    print 'creating chart', title
    DATA = []
    plt.xlabel("Sample")
    plt.ylabel(ylabel)
    fig, ax = plt.subplots()
    ax.grid(True,color='#cccccc',linestyle='solid')
    for bucket in BUCKETS:
        BUCKET_DATA = [ transformFunction(x[columnIndex]) for x in BUCKETS_STATS[bucket]]
        plt.plot(BUCKET_DATA, linestyle='solid', label=bucket, animated=True)
    plt.legend(loc='best', fontsize=10, fancybox=True)
    plt.title(title)
    plt.savefig(filename)
    plt.close()


def createCharts():
    createChart('Items per Bucket', 'Item count', 4, './out/bucket_items.png', defaultTx)
    createChart('Disk usage per Bucket', 'Gb', 2, './out/bucket_data_usage.png', memoryTx)
    createChart('Memory usage per Bucket', 'Gb', 5, './out/bucket_memory_usage.png', memoryTx)
    createChart('Fetching from disk', 'Times', 3, './out/bucket_fetch_disk.png', defaultTx)
    createChart('Operations per second per Bucket', 'Ops/s', 6, './out/bucket_ops_per_second.png', defaultTx)

for bucket in BUCKETS:
    splitBucketStats(bucket)

createCharts()