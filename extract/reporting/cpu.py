import csv
import numpy as np
import sys
from matplotlib import pyplot as plt

SESSION=sys.argv[1]
BASE_PATH=sys.argv[2]

INDEX = []
QUERY = []
DATA = []
LABELS = []
plt.figure(figsize=(16,9), dpi=150)

filename = BASE_PATH + '/' + SESSION + '_cpuavg.csv'

with open(filename, 'rb') as f:
    READER = csv.reader(f, delimiter=',')
    LINES = [x for x in READER]
    LABELS = [x[0] for i, x in enumerate(LINES) if i > 0]
    QUERY = [x[1] for i, x in enumerate(LINES) if i > 0]
    INDEX = [x[2] for i, x in enumerate(LINES) if i >0]
    DATA = [x[3] for i, x in enumerate(LINES) if i >0]

fig, ax = plt.subplots()
ax.grid(True, zorder=5)

plt.plot(QUERY, color='#256e7c', marker='', linestyle='solid', label="Query")
plt.plot(INDEX, color='#6bc6a0', marker='', linestyle='solid', label="Index")
plt.plot(DATA, color='#cddccd', marker='', linestyle='solid', label='Data')


plt.legend(loc='best')

plt.title("CPU usage")

plt.ylabel("CPU %")
plt.xlabel("Time")
	
plt.savefig("./out/cpu.png")
plt.close()