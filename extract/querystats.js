const log = require('winston'),
	fs = require('fs'),
	PWD = process.env.PWD,
	TS_LABEL = 'Time',
	REPORT_DIR = `${PWD}/extract/output/`;

class QueryStats{
	constructor(dbconnection, session, bucketName){
		this.db = dbconnection;
		this.session = session;
		this.bucketName = bucketName;
		log.info('Getting labels');
		this.dataMap = {};
		this.getLabels(); // Undo please
		this.getQueryNodes().then((nodes)=>{
			this.queryNodes = nodes;
			log.info(`queryNodes=${nodes.length}`);
			this.getBucketStats();
			this.getQueriesCount();
			this.getQueryActiveStats();
		}).catch((e)=>{
			log.error('Error getting list of query nodes');
			console.dir(e);
		});
		log.info('Generating stats for session ID', this.session);
		log.info('Getting query active stats');
		const clusterState = this.getPerformanceStats();
	}

	accResponseTime(queries){
		if (queries && Array.isArray(queries)){
			let acc = 0;
			queries.forEach((query)=>{
				if (query!==null){
					const timestring = query.elapsedTime;
					let unit = 'ms',
						factor = 1;
					if (timestring.indexOf('ms')>0){
						unit = 'ms';
					} else if (timestring.indexOf('µs')>0) {
						unit = 'µs';
						factor = .01;
					} else {
						unit = 's';
						factor = 1000;
					}
					const t = timestring.replace(unit, '') * factor;
					acc+=t;
				}
			});
			return acc;
		} else{
			return 0;
		}
	}

	saveFile(file, content){
		const fileContent = (typeof content === 'string') ? content : content.join('\n');
		fs.writeFile(`${PWD}/extract/output/${this.session}_${file}`, fileContent, 'utf8', (err)=>{
			if (err){
				log.error('Cannot create file', file);
				console.dir(err);
			} else{
				log.info('File', file, 'created');
			}
		});
	}

	async getQueriesCount(){
		const query = "select `"+this.bucketName+"`.timestamp, sum("+this.bucketName+".document.`request.completed.count`) as total from `"+this.bucketName+"` where session='"+this.session+"' and docType='queryStats' group by timestamp order by timestamp";
		const res = await this._execQueryAsync(query);
		const queryNodesCount = this.queryNodes.length;
		let ndx = 0,
			last = 0;
		const lines = [];
		lines.push(`${TS_LABEL},Queries per second,Queries per second per node`);
		res.forEach((r)=>{
			const label = new Date(r.timestamp).toLocaleTimeString();
			if (ndx!==0){
				const queriesPerSecond = (r.total - last) / 5;
				const queriesPerSecondPerNode = queriesPerSecond / queryNodesCount;
				lines.push(`${label},${queriesPerSecond},${queriesPerSecondPerNode}`);
			}
			ndx++;
			last = r.total;
		});
		this.saveFile(`queryCount.csv`, lines);
	}

	async getQueryActiveStats(){
		const all = await this._execQueryAsync("select "+this.bucketName+".* from `"+this.bucketName+"` where session='"+this.session+"' and docType='queryActive' order by timestamp;");
		const datamap = {};
		let lastTimeStamp = '';
		let lastCount = 0;
		let lastrecord = null;
		all.forEach((qs)=>{
			if (typeof datamap[qs.timestamp] !== 'object'){
				datamap[qs.timestamp] = {samples: [], aggregated: {count: 0, growth: 0, accresptime: 0, lastCount: 0, meanresptime: 0} };
			}
			const rt = this.accResponseTime(qs.document);
			datamap[qs.timestamp].aggregated.accresptime += rt;
			datamap[qs.timestamp].aggregated.count += qs.document.length; 
			datamap[qs.timestamp].samples.push( qs.document.length );
			if (qs.timestamp !== lastTimeStamp){
				lastCount = (lastrecord) ? lastrecord.aggregated.count : 0;
				datamap[qs.timestamp].aggregated.lastCount = lastCount;
				lastTimeStamp = qs.timestamp;
				lastrecord = datamap[qs.timestamp];
			} else{
				datamap[qs.timestamp].aggregated.growth = datamap[qs.timestamp].aggregated.count - lastCount;
			}
		});
		// Prints reponse times
		const lines = [];
		lines.push(`${TS_LABEL},Average query execution time`);
		this.labels.forEach((label)=>{
			const record = datamap[label];
			if (record){
				const t = new Date(label).toLocaleTimeString();
				const avg = (record.aggregated.count>0) ? record.aggregated.accresptime / record.aggregated.count : 0;
				// console.log(`${t},${record.aggregated.count},${record.aggregated.accresptime},${avg}`);
				lines.push(`${t},${avg}`);
			}
		});
		this.saveFile('responseTime.csv', lines);
		return datamap;
	}

	async getBucketStats(){
		const stats = await this._execQueryAsync("select "+this.bucketName+".* from `"+this.bucketName+"` where session='"+this.session+"' and docType='bucketstats' order by timestamp;");
		const lines = [];
		lines.push(`${TS_LABEL},Bucket,Data Used,Disk Fetches,Item Count,Memory Used,Operations per second`);
		stats.forEach((sample)=>{
			const ts = sample.timestamp;
			const label = new Date(ts).toLocaleTimeString();
			sample.document.forEach((bucket)=>{
				const bstats = bucket.basicStats;
				lines.push(`${label},${bucket.name},${bstats.dataUsed},${bstats.diskFetches},${bstats.itemCount},${bstats.memUsed},${bstats.opsPerSec}`);
			});
		});
		const bnames = (stats.length>0 ) ? stats[0].document.map( (it)=> it.name ) : [];
		this.saveFile('buckets.txt', [bnames.join(',')]);
		this.saveFile(`bucketstats.csv`,lines);
	}

	async getPerformanceStats(){
		const Stats = {};
		const stats = await this._execQueryAsync("select "+this.bucketName+".* from `"+this.bucketName+"` where session='"+this.session+"' and docType='clusterState' order by timestamp;");
		const lines = [];
		const dataLines = [];
		lines.push(`${TS_LABEL},Query CPU,Index CPU,Data CPU`);
		dataLines.push(`${TS_LABEL},Items`);
		stats.forEach((sample)=>{
			const ts = sample.timestamp;
			const nodes = sample.document.nodes;
			const entry = { query: { cpu:0, mem:0, cpuavg: 0, memavg: 0, count: 0 }, index: { cpu:0, mem:0, cpuavg: 0, memavg: 0, count: 0 }, data: { cpu:0, mem:0, cpuavg: 0, memavg: 0, hitsavg: 0,hits: 0, count: 0, items: 0}};
			nodes.forEach((node)=>{
				if (node.roles.includes('Query')){
					entry.query.count++;
					entry.query.cpu += node.cpu;
					entry.query.mem += node.mem - node.memFree;
				}
				if (node.roles.includes('Index')){
					entry.index.count++;
					entry.index.cpu += node.cpu;
					entry.index.mem += node.mem - node.memFree;
				}
				if (node.roles.includes('Data')){
					entry.data.count++;
					entry.data.cpu += node.cpu;
					entry.data.mem += node.mem - node.memFree;
					entry.data.hits += node.gethits;
					entry.data.items += node.items;
				}
			});
			entry.query.cpuavg = entry.query.cpu / entry.query.count;
			entry.query.memavg = entry.query.mem / entry.query.count;
			entry.index.cpuavg = entry.index.cpu / entry.index.count;
			entry.index.memavg = entry.index.mem / entry.index.count;
			entry.data.cpuavg = entry.data.cpu / entry.data.count;
			entry.data.memavg = entry.data.mem / entry.data.count;
			entry.data.hitsavg = entry.data.hits / entry.data.count;
			const label = new Date(ts).toLocaleTimeString();
			Stats[label] = entry;
			lines.push(`${label},${entry.query.cpuavg},${entry.index.cpuavg},${entry.data.cpuavg}`);
			dataLines.push(`${label},${entry.data.items}`)
		});
		this.saveFile(`cpuavg.csv`,lines);
		this.saveFile(`totalitems.csv`,dataLines);
	}

	async getLabels(){
		const prelabels = await this._execQueryAsync("select distinct(timestamp) from `"+this.bucketName+"` where session='"+this.session+"' order by timestamp;");
		const labels = prelabels.map((it)=>it.timestamp);
		labels.forEach((label)=>{
			this.dataMap[label] = { aggregated: {}, queryNodes: {}, index: {}, dataNodes: {}, activeQueries: {} };
		});
		this.labels = labels; // Undo please!
		return labels;
	}

	getQueryNodes(){
		return new Promise(async(resolve, reject)=>{
			const _qnodes = await this._execQueryAsync("select distinct(document.node) from `"+this.bucketName+"` where session='"+this.session+"' and `docType`= 'queryStats' ;");
			const queryNodes = [];
			_qnodes.forEach((n)=>{
				queryNodes.push(n.node);
			});
			const queryStats = await this.getQueryNodeStats(queryNodes);
			for(let i=0;i<queryStats[0].length; i++){
				let acc = 0,
					counter = 0;
				queryStats.forEach((s)=>{
					// console.log(s[i]);
					acc+=s[i]["request.completed.count"];
					counter++;
				});
				// console.log(acc);
			}
			queryStats.forEach((nodeStats)=>{
				const label = nodeStats[0]._timestamp;
				let totalRequestCount = 0;
				let totalRequestCurrent = 0;

				/**
				 * data growth
				 * queryGrowth
				 * queryActiveVariation
				 * CPUvar
				 * MemVar
				 * Avg Query time at the moment, matched with the running queries
				 */
			});
			resolve(queryNodes);
		});
	}

	async getQueryNodeStats(nodes){
		const qStats = [];
		nodes.forEach((node)=>{
			// individual promise
			qStats.push( this.db.query("select "+this.bucketName+".document.* from `"+this.bucketName+"` where session='"+this.session+"' and `docType`= 'queryStats' and document.node = '"+node+"';") );
		});
		return new Promise((resolve, reject)=>{
			Promise.all(qStats).then((response)=>{
				resolve(response);
			}).catch((e)=>{
				reject(e);
			});
		});
	}

	async _execQueryAsync(query){
		log.debug(`executing_query=${query}`);
		return new Promise((resolve, reject)=>{
			this.db.query(query).then((res)=>{
				resolve(res);
			}).catch((e)=>{
				log.debug(`queryError=${e}`);
				reject(e);
			});
		});
	}
}

module.exports = QueryStats;