const args = process.argv;
const log = require('winston');
const fs = require('fs');
const PWD = process.env.PWD;
const config = JSON.parse( fs.readFileSync(`${PWD}/config.json`, 'utf8') );

if (args.length<3){
	throw 'Not enough arguments';
}

const SESS_ID = args[2];

log.info('Processing session', SESS_ID);
log.info('Repo connection', `couchbase://${config["store-server"]}/${config["store-bucket"]}`);

const Db = require('../src/db/cb');
const db = new Db();
const QueryStats = require('./querystats');
const queryStats = new QueryStats(db, SESS_ID, config["store-bucket"]);

// Data growth