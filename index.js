const express = require('express'),
	config = require('./src/config/config'),
	PORT = config.get('port'),
	app = express(),
	bodyParser = require('body-parser'),
	cbStats = require('./src/scrapper/cbstats'),
	uuid = require('uuid'),
	log = require('winston'),
	SSE = require('express-sse'),
	sse = new SSE(),
	fs = require('fs');

let samples = 0,
	errors = 0,
	lastMetrics = {};


const views = [
	{path: "/", view: "index"}
];

app.use(bodyParser.json());
app.use(express.static('public'));
app.set('view engine', 'pug');

app.get('/api/sessionId', (req, res)=>{
	res.send({session: cbStats.sessionId});
});

app.get('/api/stats', (req, res)=>{
	res.send(lastMetrics);
});

app.get('/api/servers', (req, res)=>{
	res.send(cbStats.nodes);
});

app.get('/api/index', (req, res)=>{
	res.send(cbStats.indexes);
});

views.forEach((v)=>{
	app.get(v.path, (req, res)=>{
		res.render(v.view, {});
	});
});

app.get('/api/stream', sse.init);

app.post('/api/snapshot', (req, res)=>{
	const m = (req.body && req.body.message) ? req.body.message : 'UNKNOWN';
	cbStats.snapshot(req.body.message);
	res.send({});
});

app.post('/api/record', (req, res)=>{
	cbStats.recordingMode(true);
});

app.delete('/api/record', (req, res)=>{
	cbStats.recordingMode(false);
});

app.listen(PORT, ()=>{
	log.info(`app_port=${config.get('port')}`);
});

cbStats.on('spike', (spike)=>{
	log.debug('spikeChanged');
	sse.send({
		type: 'spike',
		data: spike
	})
});

cbStats.on('stateChanged', (servers)=>{
	log.debug('stateChanged');
	sse.send({
		type: 'servers',
		data: servers
	})
});

cbStats.on('bucketStats', (stats)=>{
	lastMetrics = stats;
	log.info('Samples', ++samples);
	sse.send({
		type: 'bucketStats',
		data: stats
	});
});

cbStats.on('indexStatus', (indexes)=>{
	sse.send({
		type: 'indexes',
		data: indexes
	});
});

cbStats.on('queryStats', (queryStats)=>{
	sse.send({
		type: 'queryStats',
		data: queryStats
	});
});

cbStats.on('queryActive', (queryStats)=>{
	sse.send({
		type: 'queryActive',
		data: queryStats
	});
});