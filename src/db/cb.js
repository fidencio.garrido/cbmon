const config = require('../config/config');
const log = require('winston');
const STORE_SERVER = config.get('store-server');
const STORE_BUCKET = config.get('store-bucket');

const Couchbase = require('couchbase');
const cluster = new Couchbase.Cluster(`couchbase://${STORE_SERVER}`);
const bucket = cluster.openBucket(STORE_BUCKET);
const N1qlQuery = Couchbase.N1qlQuery;

log.info(`storing_at=${STORE_SERVER}/${STORE_BUCKET}`);

class CbDb {
	constructor(){
		const indexes = [
			{name: 'bySession', definition: ['session']},
			{name: 'bySessionDocument', definition: ['session','docType']},
			{name: 'bySessionTime', definition: ['session','timestamp']}
		];
		log.info('creating indexes');
		indexes.forEach((ndx)=>{
			bucket.manager().createIndex(ndx.name, ndx.definition, {ignoreIfExists: true, deferred: false}, (err, resp)=>{
				if(err){
					log.error(err);
				}
			});
		});
		log.info('creating indexes - finished');
	}

	query(queryStr){
		return new Promise((resolve, reject)=>{
			bucket.query(N1qlQuery.fromString(queryStr), (err, res)=>{
				if (err){
					reject(err);
				} else{
					resolve(res);
				}
			});
		});
	}

	save(sessionId, docType, ts, docId, payload){
		const record = {
			session: sessionId,
			timestamp: ts,
			docType: docType,
			id: docId,
			document: payload
		}
		bucket.insert(docId, record, (err, item)=>{
			if (err){
				log.error(err);
			} else{
				log.debug(`saved=${docType}`);
			}
		});
	}
}

module.exports = CbDb;