const PWD = process.env.PWD,
	defaults = require(`${PWD}/config.json`);

const Config = {
	get(property, cast){
		return process.env[property] || defaults[property];
	}
}

module.exports = Config;