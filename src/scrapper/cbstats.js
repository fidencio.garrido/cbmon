const request = require('superagent'),
	config = require('../config/config'),
	CB_USER = config.get('cb-user'),
	PASS = config.get('cb-password'),
	API_PORT = config.get('api-port'),
	fs = require('fs'),
	uuid = require('uuid'),
	log = require('winston'),
	UNK = 'unknown',
	SERVER = config.get('monitored-server'),
	INTERVAL = config.get('scrap-interval'),
	TS_FIELD = '_timestamp',
	MEM_SPIKE = config.get('memspike'),
	CPU_SPIKE = config.get('cpuspike'),
	PWD = process.env.PWD,
	EventEmitter = require('events');

class StatsEmitter extends EventEmitter{};
const statsEmitter = new StatsEmitter();

let max_active = 0;

const CBClient = require("../db/cb"),
	cb_client = new CBClient();

const Stats = {}; // host as key

let saved = {
	indexes: false
}

const CB = {
	isRecording: false,
	nodes: [],
	queryNodes: null,
	indexes: null,
	apis:{
		pools: '/pools/default',
		basicStats: '/pools/default/buckets?basic_stats=true&skipMap=true',
		indexStatus: '/indexStatus'
	},
	listeners: {
		stateChanged: [],
		bucketStats: [],
		indexes: [],
		queryStats: [],
		queryActive: [],
		spike: []
	},
	lastFullMetrics: null,
	lastQueryMetrics: null,
	clusterInfo: null,
	on(event, callback){
		log.info('subscribed to ', event);
		if (typeof CB.listeners[event] !== 'undefined'){
			CB.listeners[event].push(callback);
		}
	},
	remapCBRoles(roles){
		const rolesMap = {
				index: "Index",
				kv: "Data",
				n1ql: "Query"
			},
			newRoles = [];
		roles.forEach((role)=>{
			const r = rolesMap[role] || UNK;
			newRoles.push(r);
		});
		return newRoles;
	},
	discover(ts){
		log.debug('Discovering cluster');
		CB.getAPI(CB.apis.pools).then((response)=>{
			CB.nodes = [];
			const nodes = response.nodes;
			nodes.forEach((node)=>{
				const n = {host: node.hostname, roles: CB.remapCBRoles(node.services), mem: node.memoryTotal, memFree: node.memoryFree, status: node.status, os: node.os, uptime: node.uptime, cpu: node.systemStats.cpu_utilization_rate, items: node.interestingStats.curr_items, ops: node.interestingStats.ops, gethits: node.interestingStats.get_hits};
				CB.nodes.push(n);
			});
			if (CB.nodes.length === 0){
				throw 'Monitoring requires more than 0 servers, make sure you have the right access to the monitoring APIs';
			}
			log.debug(`Cluster with ${CB.nodes.length} nodes discovered`);

			CB.clusterInfo = {timestamp: ts, nodes: CB.nodes};
			statsEmitter.emit('serverDiscovery');
		}).catch((err)=>{
			throw 'Cannot discover cluster';
		});
		CB.getIndexes(ts);
	},
	getAPI(api){
		log.debug(`calling ${api}`);
		return new Promise((resolve, reject)=>{
			request.get(`${SERVER}:${API_PORT}/${api}`).auth(CB_USER, PASS).end((err, res)=>{
				if (!err && res.statusCode === 200){
					resolve(res.body);
				} else{
					reject(err);
				}
			});
		});
	},
	getBucketsStats(ts){
		CB.getAPI(CB.apis.basicStats).then((response)=>{
			response.timestamp = ts;
			CB.lastFullMetrics = response;
			statsEmitter.emit('bucketStats');
		});
	},
	getIndexes(ts){
		CB.getAPI(CB.apis.indexStatus).then((response)=>{
			response.indexes.timestamp = ts;
			CB.indexes = response.indexes;
			statsEmitter.emit('indexStatus');
		});
	},
	getQueryNodes(){
		if (CB.queryNodes === null){
			const _queryNodes = this.nodes.filter((node)=>node.roles.includes('Query'));
			const queryNodes = [];
			_queryNodes.forEach((node)=>{
				queryNodes.push(node.host.replace(':8091', ''));
			});
			CB.queryNodes = queryNodes;
		}
		return CB.queryNodes;
	},
	getRunningQueries(session, ts){
		this.getQueryNodes().forEach((queryNode)=>{
			const url = `http://${queryNode}:8093/admin/active_requests`;
			log.debug(`query=${url}`);
			request.get(url).auth(CB_USER, PASS).end((err, res)=>{
				if (res.statusCode === 200){
					const id = uuid.v4();
					const stats = JSON.parse(res.text);
					stats._timestamp = ts;
					stats.session = session;
					stats.node=url;
					stats.node = stats.node.replace('http://','').replace(':8093/admin/active_requests', '');
					CB.activeQueries = stats;
					cb_client.save(CB.sessionId, 'queryActive', ts, uuid.v4(), CB.activeQueries);
					statsEmitter.emit('queryActive');
					// fs.writeFileSync('qstats.json', JSON.stringify(stats),'utf8');
				} else{
					log.error(res.statusCode);
				}
			});
		});
	},
	getN1ql(session, ts){
		this.getQueryNodes().forEach((queryNode)=>{
			const url = `http://${queryNode}:8093/admin/vitals`;
			log.debug(`query=${url}`);
			request.get(url).auth(CB_USER, PASS).end((err, res)=>{
				if (res.statusCode === 200){
					const id = uuid.v4();
					const stats = JSON.parse(res.text);
					stats._timestamp = ts;
					stats.session = session;
					stats.node=url;
					log.debug(stats["request.active.count"]);
					if (stats["request.active.count"]>max_active){
						max_active = stats["request.active.count"];
						log.info("new max", max_active);
						CB.emitSpike(`max_concurrent_queries=${max_active}`);
					} else{
						log.debug("current_max", max_active);
					}
					stats.node = stats.node.replace('http://','').replace(':8093/admin/vitals', '');
					CB.lastQueryMetrics = stats;
					cb_client.save(CB.sessionId, 'queryStats', ts, uuid.v4(), CB.lastQueryMetrics);
					statsEmitter.emit('queryStats');
					// fs.writeFileSync('qstats.json', JSON.stringify(stats),'utf8');
				} else{
					log.error(res.statusCode);
				}
			});
		});
	},
	updateNodeStats(nodeInfo){
		const host = nodeInfo.host;
		if (!Stats[host]){
			Stats[host] = { lastMemFree: nodeInfo.memFree, lastCPU: nodeInfo.cpu };
		}
		const last = Stats[host];
		const diffMem = last.lastMemFree - nodeInfo.memFree;
		const diffCPU = nodeInfo.cpu - last.lastCPU;
		if (diffMem > MEM_SPIKE){
			statsEmitter.emit('memspike');
		}
		if (diffCPU > CPU_SPIKE){
			statsEmitter.emit('cpuspike');
		}
		Stats[host] = { lastMemFree: nodeInfo.memFree, lastCPU: nodeInfo.cpu };
	},
	emitSpike(type){
		CB.snapshot({spike: type});
		CB.listeners.spike.forEach((listener)=>{
			listener({message: `Spike ${type}`, ts: new Date()});
		});
	},
	recordingMode(switchMode){
		this.isRecording = (switchMode === true) ? true : false;
		log.info(`recording_mode=${switchMode}`);
	},
	snapshot(message){
		const fn = PWD+'/snapshots/snapshots.txt',
			d = new Date(),
			t = d.getTime(),
			time = d.toLocaleTimeString(),
			snapshot = {
				sid: CB.sessionId,
				time: time,
				msg: message,
				ts: t
			};

		fs.appendFile(fn, JSON.stringify(snapshot)+'\n', 'utf8', ()=>{
			log.debug('snapshot saved');
		});
	},
	init(){
		CB.sessionId = uuid.v4();
		statsEmitter.on('cpuspike', ()=>{ CB.emitSpike('cpu'); });
		statsEmitter.on('memspike', ()=>{ CB.emitSpike('mem'); });
		statsEmitter.on('indexStatus', ()=>{
			if (!saved.indexes){
				cb_client.save(CB.sessionId, 'indexes', CB.indexes.timestamp, uuid.v4(), CB.indexes);
				saved.indexes = true;
			}
			CB.listeners.indexes.forEach((listener)=>{
				listener(CB.indexes);
			});
		});
		statsEmitter.on('serverDiscovery', ()=>{
			cb_client.save(CB.sessionId, 'clusterState', CB.clusterInfo.timestamp, uuid.v4(), CB.clusterInfo);
			CB.listeners.stateChanged.forEach((listener)=>{
				CB.nodes.forEach((node)=>{ CB.updateNodeStats(node); });
				listener(CB.nodes);
			});
		});
		statsEmitter.on('bucketStats', ()=>{
			cb_client.save(CB.sessionId, 'bucketstats', CB.lastFullMetrics.timestamp, uuid.v4(), CB.lastFullMetrics);
			CB.listeners.bucketStats.forEach((listener)=>{
				listener(CB.lastFullMetrics);
			});
		});
		statsEmitter.on('queryStats', ()=>{
			CB.listeners.queryStats.forEach((listener)=>{
				listener(CB.lastQueryMetrics);
			});
		});
		statsEmitter.on('queryActive', ()=>{
			CB.listeners.queryActive.forEach((listener)=>{
				listener(CB.activeQueries);
			});
		});
		setInterval(()=>{
			if (CB.isRecording){
				const ts = new Date().getTime();
				CB.discover(ts);
				CB.getBucketsStats(ts);
				CB.getN1ql(CB.sessionId,ts);
				CB.getRunningQueries(CB.sessionId, ts);
			}
		}, INTERVAL);
		log.info(`sessionId=${CB.sessionId}`);
	}
}

CB.init();

module.exports = CB;