const cb = require('couchbase'),
	log = require('winston'),
	uuid = require('uuid'),
	SIEGE_SERVER = process.env.SIEGE_SERVER || 'localhost',
	SIEGE_BUCKET = process.env.SIEGE_BUCKET || 'default';

log.info('starting siege on', SIEGE_SERVER);

const cluster = new cb.Cluster(`couchbase://${SIEGE_SERVER}`);
const bucket = cluster.openBucket(SIEGE_BUCKET);
const n1qlQuery = cb.N1qlQuery;

const docsInsert = 5000;

bucket.manager().createIndex("byName", ["name"], {ignoreIfExists: true, deferred: false}, (err, resp)=>{
	log.info('index created');
});

function multiInsert(){
	for (let i=0; i<docsInsert; i++){
		bucket.insert(uuid.v4(), {
			name: `name${i}`,
			lastName: `lastName${i}`,
			age: 22
		}, (err, doc)=>{
			if (err){
				console.log(err);
			} else{
				//log.info(doc);
			}
		});
	}
}

const _query = "select name from default where name='name1' limit 5";
let qCount = 0;
function query(){
	log.info("query", ++qCount);
	bucket.query(n1qlQuery.fromString(_query), (err, res)=>{
		if (err){
			console.log(err);
		} else{
			console.log(res);
		}
	});
}

for(let i=0; i<10000; i++){
	setTimeout(multiInsert, i*1500);
	if (i % 3 === 0){
		setTimeout(query, i*3);
	}
}
console.log('done');
// 92 168 123 172
