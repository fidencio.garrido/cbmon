const es = new EventSource('/api/stream');

const Client = {
	getApi(api, req_method){
		return new Promise((resolve, reject)=>{
			$.ajax({
				method: req_method || 'get',
				url: api,
				contentType: 'application/json',
				success(data){
					resolve(data);
				},
				error(err){
					reject(err);
				}
			})
		});
	}
}

const handlersMap = {
	servers: 'updateServers',
	bucketStats: 'updateBucketStats',
	indexes: 'updateIndexes',
	queryStats: 'updateQueryStats',
	queryActive: 'updateActiveQueries',
	spike: 'updateLog'
}

const MAX_QUERY_HISTORY = 300;

let queryHistoryNdx = 0;

const webapp = new Vue({
	el: '#app',
	data:{
		currentPanel: 'stats',
		sessionId: '',
		stats: null,
		nodes: null,
		indexes: [],
		jobs: [],
		log: [],
		activeQueries: [],
		queryHistory: [],
		servers: [],
		slaves: [],
		topics: [],
		topic: '',
		querystats: []
	},
	created(){
		es.onmessage = (event)=>{
			const q = JSON.parse(event.data);
			const type = q.type || null;
			if (q.type !== null){
				const h = handlersMap[q.type];
				this[h](q.data);
			}
		};
		this.getSessionId();
		this.getServers();
		this.getIndexes();
	},
	filters: {
		round(i){
			return i.toLocaleString();
		},
		toGb(n){
			return n / 1073741824; // 1024 / 1024 / 1024
		}
	},
	methods: {
		updateServers(servers){
			this.servers = servers;
			const ts = new Date();
			const qs = this.servers.filter( (server)=> server.roles.includes('Query') );
			const d = this.servers.filter( (server)=>server.roles.includes('Data'));
			const n = this.servers.filter( (server)=>server.roles.includes('Index'));
			this.addData(ts, 'Query', qs);
			this.addData(ts, 'Data', d);
			this.addData(ts, 'Index', n);
		},
		updateActiveQueries(activeQueries){
			if (typeof activeQueries === "object" && Array.isArray(activeQueries) && activeQueries.length>0){
				this.activeQueries = activeQueries;
				const self = this;
				activeQueries.forEach((query)=>{
					let ndx = queryHistoryNdx++;
					if (ndx>=MAX_QUERY_HISTORY){
						ndx = 0;
						queryHistoryNdx = 0;
					}
					self.queryHistory[ndx]=query;
				});
			} else{
				this.activeQueries = [];
			}
		},
		updateBucketStats(stats){
			this.addStats(stats);
		},
		updateIndexes(indexes){
			this.indexes = indexes;
		},
		updateLog(entry){
			this.log.push(entry);
		},
		updateQueryStats(queryStats){
			let found = false,
				ndx = 0;
			const self = this;
			this.querystats.forEach((node)=>{
				if (node.node === queryStats.node){
					self.querystats[ndx] = queryStats;
					found = true;
				}
				ndx++;
			});
			if (!found){
				this.querystats.push(queryStats);
			}
		},
		getSessionId(){
			Client.getApi('/api/sessionId').then((response)=>{
				this.sessionId = response.session;
			});
		},
		getIndexes(){
			Client.getApi('/api/index').then((indexes)=>{
				this.updateIndexes(indexes);
			});
		},
		getServers(){
			Client.getApi('/api/servers').then((servers)=>{
				this.updateServers(servers);
			}).catch((err)=>{
				console.dir(err);
			});
		},
		addData(ts, role, source){
			let m = 0,
				cpu = 0,
				counter = 0;
			source.forEach((node)=>{
				cpu += node.cpu;
				m += (node.mem - node.memFree);
				counter++;
			});
			Chart.addData( new Date(), role, cpu / counter, m / counter );
		},
		addStats(q){
			this.stats = q;
		},
		snapshot(message){
			$.ajax({
				method: 'post',
				url: '/api/snapshot',
				contentType: 'application/json',
				data: JSON.stringify({message: message}),
			});
		},
		test(isStart){
			let _method = 'post';
			let message = 'Session started';
			if (isStart !== true){
				_method = 'delete';
				message = 'Session end';
			}
			$.ajax({
				method: _method,
				url: '/api/record',
				contentType: 'application/json'
			});
			this.snapshot(message);
		},
		job(slaveId){
			const id = Math.floor( Math.random() * 1000 ),
				self = this;
			console.log("new job for slave id ", slaveId, "id", id);
			$.ajax({
				method: 'post',
				url: '/api/job/'+id,
				contentType: 'application/json',
				data: JSON.stringify({source: slaveId}),
				success(){
					self.jobs.push({ id: id, worker: slaveId, status: 'New' });
				},
				error(err){
					alert(err);
				}
			});
		},
		getSlaves(){
			const self = this;
			$.get('/api/slaves', (slaves)=>{
				self.slaves = slaves;
			});
		},
		ping(){
			$.ajax({
				method: 'post',
				url: '/api/ping'
			});
		},
		subscribe(){
			if (this.topic.trim()!==''){
				const self = this;
				$.ajax({
					url: '/api/topic/'+this.topic,
					method: 'post',
					success(){
						self.topics.push(self.topic);
					},
					error(e){
						alert('Cannot subscribe to '+ self.topic);
						console.log(e);
					}
				})
			} else{
				alert('Topic name cannot be empty');
			}
		}
	}
});